JFLAGS = -g
JC = javac
JVM = java

.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	net/Main.java \
	net/AckType.java \
	net/CCSThread.java \
	net/FirewallTable.java \
	net/Frame.java \
	net/SwitchThread.java \
	net/Switch.java \
	net/SwitchTask.java \
	net/Node.java \
	net/NodeFileIO.java

MAIN = net.Main

classes: $(CLASSES:.java=.class)

default: classes

run: classes
	$(JVM) $(MAIN) 2 2

clean:
	$(RM) net/*.class
