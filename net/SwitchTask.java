package net;

/**
 * Enum that details potential tasks of the switches
 */
public enum SwitchTask {
    SEND,
    RECEIVE,
    FLOOD
}
