package net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static java.lang.Character.isDigit;

/**
 * Class that handles the functionality of the node
 */
public class Node extends Thread {
	ServerSocket serverSocket;
	Socket socket;
	private InetAddress inetAddress;
	private int listeningPort;
	private int switchPort;
	private int netID;
	private int nodeID;
	private int framesSent;
	private int framesReceived;
	private int dataCount;
	private boolean[] acceptedData;
	private Switch star;
    List<String> fileData;
    private HashMap<Integer, Frame> frameBuffer;

    /**
	 * Empty constructor
	 */
	Node () {}

	/**
	 * creates a node
	 * @param host use localhost
	 * @param nodeID should be unique
	 * @param switchPort port of switch
	 * @param listeningPort port to listen on for data
	 */
	public Node(String host, int netID, int nodeID, int switchPort, int listeningPort, Switch star) {
		frameBuffer = new HashMap<>();
		framesSent = 0;
		framesReceived = 0;
		this.star = star;
		this.listeningPort = listeningPort;
		this.switchPort = switchPort;
		this.nodeID = nodeID;
		this.netID = netID;
		try {
			this.inetAddress = InetAddress.getByName(host);
		} catch (IOException e) {
			System.out.println("node"+ nodeID + " cannot find host\n" + e.getMessage());
		}
		// keeps track of nodes that have sent it data.
		acceptedData = new boolean[star.allLinks.length * (star.allLinks.length - 1)];
		for (int i = 0; i < acceptedData.length; i++) {
			acceptedData[i] = false;
		}
	}

	public int getListeningPort(){
		return listeningPort;
	}

	public int getDataCount() {
	    return ++dataCount;
    }

	/**
	 * sets a new listening port number
	 * @param port number to set the port to
	 */
	public void setListeningPort(int port) {
		this.listeningPort = port;
	}

	/**
	 * sets a new switchPort to connect to
	 * @param port number to set the port to
	 */
	public void setSwitchPort(int port) {

		this.switchPort = port;
	}

	/**
	 * Connects to the switch
	 * @return true if a connection was established, false otherwise
	 */
	private synchronized boolean connectToSwitch() {
		try {
			socket = new Socket(this.inetAddress, star.getPortNumber());
			return true;
		} catch (IOException e) {
			System.out.println("Node" + netID + "_" + nodeID +" failed to connect to the switch\nTrying again in 2 seconds\n" + e.getMessage());
			try {
				sleep(2000);
			} catch (InterruptedException i) {
				System.out.println("Node" + netID + "_" + nodeID + " sleep time interrupted\n" + i.getMessage());
			}
			return false;
		}
	}

	/**
	 * Node thread's run functions
	 * sends all data then waits to receive.
	 */
	public void run() {
		send();

		while (listeningPort == 0) {
			try {
				sleep(1 * 1000);
			} catch (InterruptedException e) {
				e.getMessage();
			}
		}
		receive();
		try {
			sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Node thread's send function
	 * send data to switch
	 */
	public void send() {
	    Random rand = new Random();
		int error = rand.nextInt(20);

	    // read node file
		fileData = NodeFileIO.readInputFile("./node/node" + netID + "_" + nodeID + ".txt");

		// create and send frames
		for (int i = 0; i < fileData.size(); i++) {
			Frame f = new Frame(fileData.get(i), netID, nodeID, getDataCount());
			frameBuffer.put(f.getDataID(), new Frame(f.getFrame()));

			// TODO: change this to the appropriate percentage when crc ack is working
            if (error >= 20) {
                // create erroneous frame
                int oldSize = f.getSize();
                int oldCRC = f.getCRC();

                f.setData("");
                f.setCRC(oldCRC);
                f.setSize(oldSize);
            }

			byte[] byteFrame = f.getFrame();
			boolean isConnected;
			do {
				isConnected = connectToSwitch();
			} while (!isConnected);

			NodeFileIO.writeBytesToSocket(socket, byteFrame);
			NodeFileIO.closeSocket(socket);
		}

		framesSent = fileData.size();
	}

	/**
	 * accepts incoming connection
	 * @param socket socket that connects.
	 * @return the socket that connected
	 */
	private Socket acceptConnection(Socket socket) {
		try {
			socket = serverSocket.accept();
			return socket;
		} catch (IOException e) {
			System.out.println("Node" + netID + "_" + nodeID + " failed to accept connection\n" + e.getMessage());
			try {
				sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			return null;
		}
	}

	/**
	 * Node thread's receive function
	 * receive data from switch, make sure data belongs to this node (right dest)
	 */
	public void receive() {
		int acksReceived = 0;
		int attempts = 10;
		Socket socket = null;
		byte[] frame = new byte[Frame.framesize];
		Frame f = null;
		int acksSent = 0;
		try {
			serverSocket = new ServerSocket(listeningPort);
		} catch (IOException e) {
			e.printStackTrace();
		}


		// create timeout condition for never receiving an ack
		while (acksReceived < framesSent) {
			socket = acceptConnection(new Socket());

			if (socket != null) {
				// read frame into object
				if ((frame = NodeFileIO.readBytesFromSocket(socket, frame)) != null) {
					f = frameToObject(frame);
				}
			} else {
				attempts--;
				if (attempts < 0)
					return;
			}
			// data was read from the socket
			if (f != null) {
				// check to make sure the node is the intended receiver
				if (f.getNodeDest() != nodeID) {
					System.out.println("node " + netID + "_" + nodeID + " REJECTING frame [dest " + f.getNetDest() + "_" + f.getNodeDest() + "]: does not belong");
					continue;
				}
				// this code may have more than a few problems...
				// frame is an ack
				if (f.getSize() == 0) {
					if (f.getAckType() == AckType.POSITIVE) {
						System.out.println("node " + netID + "_" + nodeID + " ACCEPTING ACK:" + f.getAckType() + " [src " + f.getNetSrc() + "_" + f.getNodeSrc() + "]");
						acksReceived++;
						star.ackCount++;
						frameBuffer.remove(f.getDataID());
					} else if (f.getAckType() == AckType.FIREWALL) {
						frameBuffer.remove(f.getDataID());
						acksReceived++;
					} else if (f.getAckType() == AckType.CRCERR || f.getAckType() == AckType.TIMEOUT) {
						// resend frame
						System.out.println("node " + netID + "_" + nodeID + " ACCEPTING ACK:" + f.getAckType() + " [src " + f.getNetSrc() + "_" + f.getNodeSrc() + "]");
						byte[] byteFrame = frameBuffer.get(f.getDataID()).getFrame();
						boolean isConnected;
						do {
							isConnected = connectToSwitch();
						} while (!isConnected);

						NodeFileIO.writeBytesToSocket(socket, byteFrame);
						NodeFileIO.closeSocket(socket);
					}
				}
				// frame is data
				else {
					// increment acksSent and print data only if the data wasn't corrupted
					if (f.checkCRC() == AckType.POSITIVE) {
						sendAck(f, AckType.POSITIVE);
						System.out.println("node " + netID + "_" + nodeID + " ACCEPTING frame [src " + +f.getNetSrc() + "_" + f.getNodeSrc() + "]" + "[dest " + f.getNetDest() + "_" + f.getNodeDest() + "]");
						acksSent++;
						NodeFileIO.writeOutputFile("./node/node" + netID + "_" + nodeID + "output.txt", f.dataToString());
					} else if (f.checkCRC() == AckType.CRCERR) {
						// send ack: CRC error
						sendAck(f, AckType.CRCERR);
					}
				}
			}
		}
	}

	/**
	 * note: The creation of the ack should be put into Frame class
	 * sends an ACK frame upon receiving data
	 * @param frame the frame that we're acknowledging receiving.
	 * @return true if Positive ack, False is CRC error ack
	 */
	private boolean sendAck(Frame frame, AckType type) {
		Frame ack = Frame.createAck(frame, type);

		boolean isConnected;
		do {
			isConnected = connectToSwitch();
		} while (!isConnected);
		NodeFileIO.writeBytesToSocket(socket, ack.getFrame());
		NodeFileIO.closeSocket(socket);

		if (type == AckType.POSITIVE) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * wrapper function: converts a byte array representation of a frame into a Frame object
	 * @param frame the byte array to convert
	 * @return The byte array as a Frame object
	 */
	private Frame frameToObject(byte[] frame) {
		return new Frame(frame);
	}
 }
