package net;

import java.io.Serializable;
import java.util.*;

/**
 * Implementation of the firewall table
 */
public class FirewallTable implements Serializable {

	private List<Integer> globalFirewallTable;
	private Map<Integer, Integer> localFirewallTable;

	/**
	 * Constructor for the firewall table the simply initializes the global and local tables.
	 */
	FirewallTable() {
		globalFirewallTable = new ArrayList<>();
		localFirewallTable = new HashMap<>();
	}

	/**
	 * Constructor for the firewall table. Reads lines and puts them in the proper table
	 * @param filepath The path to the file with the firewall table info
	 */
	FirewallTable(String filepath) {
		// reads file 1 line per list
		List<String> lines = NodeFileIO.readInputFile(filepath);

		globalFirewallTable = new ArrayList<>();
		localFirewallTable = new HashMap<>();
		
		for(String s: lines) {
			String temp[] = (s.substring(0, s.indexOf(","))).split("_");
			if (temp[1].equals("*")){
				globalFirewallTable.add(Integer.parseInt(temp[0]));
			}
			else {
				localFirewallTable.put(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
			}
		}
	}

	/**
	 * @return The global firewall table
	 */
	public List<Integer> getGlobalFirewallTable() {
		return globalFirewallTable;
	}

	/**
	 * @return The local firewall table
	 */
	public Map<Integer, Integer> getLocalFirewallTable(){
		return localFirewallTable;
	}

	/**
	 * checks a network and node_2 ID against the local firewall table
	 * @param netID The destination network ID
	 * @param nodeID The destination node_2 ID
	 * @return true if blocked, false otherwise
	 */
	public boolean checkLocalFirewallTable(int netID, int nodeID) {
		return (localFirewallTable.containsKey(netID) && localFirewallTable.get(netID) == nodeID);
	}

	/**
	 * checks a network ID against the global firewall table
	 * @param netID The destination network ID
	 * @return true if blocked, false otherwise
	 */
	public boolean checkGlobalFirewallTable(int netID) {
		return globalFirewallTable.contains(netID);
	}
}
