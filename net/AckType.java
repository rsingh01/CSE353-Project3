package net;

/**
 * types of acknowledgements
 */
public enum AckType {
    TIMEOUT,
    CRCERR,
    FIREWALL,
    POSITIVE
}
