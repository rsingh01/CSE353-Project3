package net;


public class Main {
    /**
     * Initializes the arrays of switches and the nodes in each, and activates the threads in the switches.
     * @param args command-line arguments
     */
    public static void main(String args[]) {
        // parse args for number of nodes and arms
        // create switch objects
        // create node objects

        int nodeNumber;
        int armNumber;
        Node[][] nodes;
        Switch[] arms;
        Switch CCS;
        int serverPort = 10000;

        //usage on how to run the program
        if (args.length == 0) {
            System.out.println("Usage: java Main [arm number] [node number]");
            System.out.println("argument integer [node number] should from 2-16");
            System.out.println("argument integer [arm number] should from 2-16");
            return;
        }

        //get number of arms and nodes that the program should work with
        armNumber = Integer.parseInt(args[0]);
        nodeNumber = Integer.parseInt(args[1]);
        nodes = new Node[armNumber][nodeNumber];
        arms = new Switch[armNumber];

        if (armNumber > 16 || armNumber < 2) {
            System.out.println("Usage: java Main [arm number] [node number]");
            System.out.println("argument integer [node number] should from 2-16");
            System.out.println("argument integer [arm number] should from 2-16");
            return;
        }

        if (nodeNumber > 16 || nodeNumber < 2) {
            System.out.println("Usage: java Main [arm number] [node number]");
            System.out.println("argument integer [node number] should from 2-16");
            System.out.println("argument integer [arm number] should from 2-16");
            return;
        }

        // create an array of CAS'
        // create an array of threaded nodes for each CAS
        // run each of those.
        for (int i = 0; i < armNumber; i++) {
            arms[i] = new Switch(serverPort + i, i + 1, nodes[i]);

            for (int j = 0; j < nodeNumber; j++) {
                nodes[i][j] = new Node("localhost", i + 1, j + 1, arms[i].getPortNumber(), (5000 + (16 * i)) + j, arms[i]);
                nodes[i][j].start();
            }
        }

        // initialize CCS and threads
        // initialize CCS receiving thread
        CCS = new Switch(serverPort - 1, arms, "./firewall/firewall.txt");

        Thread CCSReceiver = new Thread(CCS);
        CCSReceiver.setName("ccsReceiver");
        CCSReceiver.start();

        //initialize arm threads
        for (int i = 0; i < armNumber; i++) {
            // init receiving thread of each CAS
            Thread receiver = new Thread(arms[i]);
            receiver.setName("CAS" + i + "receiver");
            receiver.start();
        }

        try {
            Thread.currentThread().sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // initialize CCS sending thread
        CCS.setSending();
        Thread CCSSender = new Thread(CCS);
        CCSSender.setName("ccsSender");
        CCSSender.start();

        for (int i = 0; i < armNumber; i++) {
            // init sending thread of each CAS
            arms[i].setSending();
            Thread sender = new Thread(arms[i]);
            sender.setName("CAS" + i +"sender");
            sender.start();
        }
    }
}

