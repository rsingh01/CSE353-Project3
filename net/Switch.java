package net;

import java.io.*;
import java.net.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * The switch class listens for connections and spawns threads to deal with requests. It also maintains
 * a queue of data to be forwarded.
 */
public class Switch implements Runnable {

	public static int CCSport;
	public BlockingQueue<byte[]> queue; /* Made this non-static to allow for multiple switches.						 */
	 									/* The change requires an instance of the switch to be passed to its threads */
	public Node[] allLinks; // an array filled with all node port numbers in network. use this for flooding. Main should pass this list in.
	public Switch[] allCAS; // an array filled with all the CAS port numbers in network.

	private Socket socket;
	private ServerSocket serverSocket;
	private int portNumber; // listening port for this switch
	private int netID;

	public FirewallTable LFT;

	public Map<Integer, Integer> port2node; /* The switching table consists of both of these maps  */
	public Map<Integer, Integer> node2port;

	public Map<Integer, Socket>  threadID2socket; /* The sockets used by the switch thread, used to clean up sockets */

	private boolean waitTimedOut;
	private boolean isSending;
	public int ackCount;

	public static FirewallTable firewallTable;
	
	boolean isCCS;
	
	/**
	 * constructor to initialize the CAS
	 * @param portNumber listening port number of the switch
	 * @param netID network id of this switch
	 * @param allLinks all nodes in this network
	 */
	Switch(int portNumber, int netID, Node[] allLinks) {
		isCCS = false;
		waitTimedOut = false;
		isSending = false;
		this.netID = netID;
		this.allLinks = allLinks;
		this.portNumber = portNumber;
		this.port2node = new HashMap<>();
		this.node2port = new HashMap<>();
		this.threadID2socket = new HashMap<>();
		queue = new ArrayBlockingQueue<byte[]>(1024);

	}

	/**
	 * constructor for CCS
	 * @param portNumber listening port of the switch
	 * @param allLinks all the CAS' in this network
	 * @param firewallFilepath
	 */
	Switch(int portNumber, Switch[] allLinks, String firewallFilepath) {
		
		isCCS = true;
		firewallTable = new FirewallTable(firewallFilepath);

		waitTimedOut = false;
		isSending = false;
		this.allCAS = allLinks;
		CCSport = this.portNumber = portNumber;
		this.port2node = new HashMap<>();
		this.node2port = new HashMap<>();
		this.threadID2socket = new HashMap<>();
		queue = new ArrayBlockingQueue<byte[]>(1024);


	}

	/**
	 * initializes the network
	 */
	private void initNetwork() {
		
		//if the Switch being initialized is a CCS, we iterate through the
		//CAS array and initialize each CAS, connect to them, and write
		//the firewall table to them
		if (isCCS) {
			System.out.println("Initializing CCS");

			for (int i = 0; i < allCAS.length; i++) {
				System.out.println("Initializing CAS" + i);
				while(!connectToCAS(allCAS[i].getPortNumber(),1));

				try { // send firewall table to CASs
					ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
					outputStream.writeObject(firewallTable);
					outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					System.out.println("This?" + e.getMessage());
				} finally {
					try {
						socket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		else {
			//otherwise we initialize a normal CAS
			int test = 0;
			openListeningPort();
			try {
				Socket CCSlink = serverSocket.accept();
				//ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(CCSlink.getOutputStream()));
				ObjectInputStream inputStream = new ObjectInputStream(CCSlink.getInputStream());
				LFT = (FirewallTable) inputStream.readObject();
				System.out.println(++test);
				inputStream.close();
				CCSlink.close();
				serverSocket.close();
			} catch (IOException e) {
				System.out.println(this.getPortNumber() + " "+ e.getMessage());
			} catch (ClassNotFoundException c) {
				System.out.println("This?2" + c.getMessage());
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * getter for network ID
	 * @return network ID
	 */
	public int getNetID(){return netID;}

	/**
	 * connects the CCS to a CAS
	 * @param port port of the CAS
	 * @param waitTime amount of time to wait between connection attempts
	 * @return true if the connection is made, otherwise false
	 */
	private boolean connectToCAS(int port, int waitTime) {

		try {
			socket = new Socket(InetAddress.getByName("localhost"), port);
			return true;

		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("CCS failed to connect" + e.getMessage());
			try {
				Thread.sleep(waitTime * 1000);
			} catch (InterruptedException i) {
				System.out.println("CCS sleep time interrupted\n" + i.getMessage());
			}
			return false;
		}
	}

	/**
	 * sets the flag to indicate the switch should send
	 */
	public void setSending() {
		this.isSending = true;
	}

	/**
	 * run function for the CAS/CCS switches
	 * start of the switch threads
	 */
	@Override
	public void run() {

		//start and run server
		if (isSending) {
			startSending();
		}
		else {
			//initNetwork();
			startReceiving();
		}

		//startSwitch();
	}

	/**
	 * simple getter for the port number
	 * @return the port number
	 */
	public int getPortNumber(){
		return portNumber;
	}

	/**
	 * sets up the switch to send data
	 */
	private void startSending() {
		int attempts = 10;
		while (queue.isEmpty()) {
			try {
				Thread.currentThread().sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		while (!queue.isEmpty()) {
			byte[] frame = takeFromQueue();
			if(isCCS){
				new CCSThread(new Socket(), SwitchTask.SEND, this, frame).start();
			}
			else {
				new SwitchThread(new Socket(), SwitchTask.SEND, this, frame).start();
			}
			while (attempts > 0) {
				if (queue.isEmpty()) {
					try {
						attempts--;
						Random rand = new Random();
						int mult = rand.nextInt(10) + 1;
						Thread.currentThread().sleep(mult * 300);
					} catch (InterruptedException e) {
						e.getMessage();
					}
				} else {
					attempts = 10;
					break;
				}
			}
		}
		if(isCCS){
			System.out.println("No recent data to send, CCS sender closing");
		}
		else {
			System.out.println("No recent data to send, CAS" + this.getNetID() + " closing");
		}
	}

	/**
	 * opens the listening port and sets up the switch for receiving
	 */
	private void startReceiving() {
		openListeningPort();
		int attempts = 5;
		Socket node = acceptConnection(new Socket());
		while (!waitTimedOut) {
			if (node != null && node.isConnected()) {
				// begin a Thread to add frame to queue
				if (isCCS) {
					new CCSThread(node, SwitchTask.RECEIVE, this).start();
				}
				else {
					new SwitchThread(node, SwitchTask.RECEIVE, this).start();
				}
				// the spawned SwitchThread must close its own socket.
			}
			node = acceptConnection(new Socket());
		}
	}

	/**
	 * takes the next item from the global queue
	 * @return the frame which we're taking from the queue
	 */
	private synchronized byte[] takeFromQueue(){
		byte[] frame = new byte[512];
		try {
			if (!queue.isEmpty())
				frame = queue.take();
			else
				return null;
		} catch (InterruptedException e) {
			e.getMessage();
		}
		return frame;
	}

	/**
	 * Adds a node to the switching table.
	 * @param port port of node
	 * @param nodeID id of node
	 * @return true if node was added, false if node was already in table.
	 */
	public synchronized boolean addToSwitchingTable(int port, int nodeID) {
		boolean addedToSwitchingTable = false;
		if (!node2port.containsValue(port)) {
			node2port.put(nodeID, port);
			addedToSwitchingTable = true;
		}
		if (!port2node.containsValue(nodeID)) {
			port2node.put(port, nodeID);
			addedToSwitchingTable = true;
		}
		return addedToSwitchingTable;
	}

	/**
	 * recieve ack and close sockets used to send
	 * @param task Checks if the task a flood before closing sockets.
	 * @param frame The ACK frame
	 * @param floodSockets The array of sockets used to flood the nodes
	 */
	public void receiveACK(SwitchTask task, Frame frame, Socket floodSockets[]) {
		for (int i = 0; i < floodSockets.length; i++) {
			if (floodSockets[i] != null && !floodSockets[i].isClosed()) {
				try {
					floodSockets[i].close();
				} catch (IOException e) {
					System.out.println("Unable to close socket connected to node " + port2node.get(floodSockets[i].getPort()));
				}
			}
		}
	}



	/**
	 * opens the listening port for the switch and sets the time it was opened
	 */
	private void openListeningPort(){
		// set switch start time
		//setStartTime();
		try {
			serverSocket = new ServerSocket(portNumber);
			serverSocket.setSoTimeout(15000);
		} catch (IOException e) {
			System.out.println("Switch: Failed to open listening port\n" + e.getMessage());
		}
	}

	/**
	 * accepts a connection
	 * @param node new socket
	 * @return the socket of the connecting node on success, null on failure
	 */
	private Socket acceptConnection(Socket node) {
		try {
			node = serverSocket.accept();
			return node;
		}
		catch (SocketTimeoutException timeoutException) {
			System.out.println("No recent connection requests, switch receiver closing");
			waitTimedOut = true;
			return null;
		}
		catch (IOException ioe) {
			System.out.println("Unable to accept connection: " + ioe.getMessage());
			return null;
		}
	}
 }