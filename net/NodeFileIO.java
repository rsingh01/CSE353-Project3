package net;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that handles the file I/O of the nodes
 */
public final class NodeFileIO {
    /**
     * reads all lines in a file and stores each line as a string in an array list
     * @param filepath the path of the file to read
     * @return a List of strings, one per line in the file
     */
    public static List<String> readInputFile(String filepath) {
        BufferedReader reader;
        List<String> lines = new ArrayList<>();
        try {
            reader = new BufferedReader(new FileReader(filepath));
            String line;

            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("Failed to read file: " + filepath + "\n" + e.getMessage());
        }
        return lines;
    }

    /**
     * writes string to specified filepath
     * @param filepath filepath of file to write to
     * @param data string to write
     */
    public static void writeOutputFile(String filepath, String data){
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(filepath, true));
            data += "\n";
            writer.write(data);
            writer.flush();
            writer.close();
        }catch (IOException e) {
            System.out.println("Failed to write outfile: " + filepath + "\n" + e.getMessage());
        }
    }

    /**
     * writes bytes to socket
     * @param socket socket to write to
     * @param frame byte array to write
     */
    public static void writeBytesToSocket(Socket socket, byte[] frame) {
        OutputStream outputStream;
        try {
            outputStream = socket.getOutputStream();
            outputStream.write(frame);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            System.out.println("Failed to write to output stream\n" + e.getMessage());
        }
    }

    /**
     * reads bytes from socket
     * @param socket socket to read from
     * @param frame byte array to store bytes
     * @return the byte array with the data from the socket
     */
    public static byte[] readBytesFromSocket(Socket socket, byte[] frame) {
        try {
            socket.getInputStream().read(frame);
        } catch (IOException e) {
            System.out.println("Failed to read from output stream\n" + e.getMessage());
        }
        return frame;

    }
    
    /**
     * closes a socket (not a Serversocket).
     * @param socket, the socket to close
     */
    public static void closeSocket(Socket socket) {
        try {
            if (socket != null && !socket.isClosed())
                socket.close();
        } catch (IOException e) {
            System.out.println("Failed to close socket on port " + socket.getPort() +"\n"+ e.getMessage());
        }
    }
    
    /**
     * closes a ServerSocket
     * @param socket, the socket to close
     */
    public static void closeSocket(ServerSocket serverSocket) {
        try {
            if (serverSocket != null && !serverSocket.isClosed())
                serverSocket.close();
        } catch (IOException e) {
            System.out.println("Failed to close server socket\n"+ e.getMessage());
        }
    }
}
