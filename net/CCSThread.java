package net;


import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

/**
 * Class that handles the duties of the CCS
 */
public class CCSThread extends Thread {
	private SwitchTask task;	// the task assigned to this thread
	private Socket nodeSocket;	// the socket of the node this thread is serving
	private Socket[] nodeSockets; // used for flooding
	private InetAddress inetAddress;
	private Switch controller; // the switch that spawned this thread
	private static long switchThreadID = 0; // Incremented and assigned to threadID with each SwitchThread that starts
	private static int nodeListeningPort = 6000;
	private int nodePort;
	private long threadID; // this data is added to the frame, 8 bytes between size and data.
	byte[] frame; // temp frame storage

	// add firewall table to constructors

	/**
	 * Constructor
	 * @param nodeSocket the socket that connects the CCS to its CASs
	 * @param task the task of the CCS
	 * @param controller the switch that this thread belongs to
	 */
	CCSThread(Socket nodeSocket, SwitchTask task, Switch controller) {
		this.nodePort = ++nodeListeningPort;
		this.threadID = ++switchThreadID;
		this.task = task;
		this.nodeSocket = nodeSocket;
		this.controller = controller;
		try {
			inetAddress = InetAddress.getByName("localhost");
		}catch (IOException e) {
			e.getMessage();
		}
		
	}

	/**
	 * Constructor
	 * @param nodeSocket the socket that connects the CCS to its CASs
	 * @param task the task of the CCS
	 * @param controller the switch that this thread belongs to
	 * @param frame the data frame
	 */
	CCSThread(Socket nodeSocket, SwitchTask task, Switch controller, byte[] frame) {
		this.nodePort = ++nodeListeningPort;
		this.threadID = ++switchThreadID;
		this.task = task;
		this.nodeSocket = nodeSocket;
		this.controller = controller;

		try {
			inetAddress = InetAddress.getByName("localhost");
		}catch (IOException e) {
			e.getMessage();
		}
		this.frame = new byte[Frame.framesize];
		System.arraycopy(frame, 0, this.frame, 0, frame.length);
	}

	/**
	 * run function for the threads
	 * sends or receives based on the task
	 */
	public void run() {
		switch (task) {
			case RECEIVE:
				receive();
				NodeFileIO.closeSocket(nodeSocket);
				break;
			case SEND:
				send();
		}
	}

	/**
	 * takes the next item from the switch's buffer
	 * @return the next byte array from the switch's buffer, null if buffer is empty
	 */
	private synchronized byte[] takeFromQueue(){
		byte[] frame = new byte[Frame.framesize];
		try {
			if (!controller.queue.isEmpty())
				frame = controller.queue.take();
			else
				return null;
		} catch (InterruptedException e) {
			e.getMessage();
		}
		return frame;
	}

	/**
	 * puts a byte array (frame) into the switch's buffer
	 * @param frame frame to put in the buffer
	 */
	private synchronized void putInQueue(byte[] frame) {
		try {
			controller.queue.put(frame);
		} catch (InterruptedException e) {
			e.getMessage();
		}
	}



	/**
	 * accepts incoming connection
	 * @param socket socket that connects.
	 * @return the socket that connected
	 */
	private Socket acceptConnection(Socket socket, ServerSocket serverSocket, int port) {

		try {
			serverSocket = new ServerSocket(port);
			socket = serverSocket.accept();
			return socket;
		} catch (IOException e) {
			System.out.println("CCSTHREAD" + threadID + " failed to accept connection\n" + e.getMessage());
			return null;
		}
	}


	/**
	 * receives data from a node, adds to the switch's buffer
	 */
	public void receive() {
		byte[] frame = new byte[Frame.framesize];
		frame = NodeFileIO.readBytesFromSocket(nodeSocket, frame); // place data into temporary byte array
		Frame f = new Frame(frame);
		// do firewall checks here
		// convert frame to class and check against firewall table
		
		// add src to switching table and print confirmation.
		if (!controller.node2port.containsValue(f.getNetSrc())) {
			if (f.getNetSrc() > 0)
				nodePort = controller.allCAS[f.getNetSrc() - 1].getPortNumber();

			if (controller.addToSwitchingTable(nodePort, f.getNetSrc())) {
				System.out.println("network " + f.getNetSrc() + " added to CCS switching table at port " + controller.node2port.get(f.getNetSrc()));
			}
		}
		if (f.getSize() == 0) {
			System.out.println("CCS received ACK from node" + f.getNetSrc() + "_" + f.getNodeSrc() + " for node" + f.getNetDest() + "_" + f.getNodeDest() + " type: " + f.getAckType());
		}
/*
		else if (controller.firewallTable.checkGlobalFirewallTable(f.getNetSrc())) {
			Frame ack = Frame.createAck(f, AckType.FIREWALL);
			putInQueue(ack.getFrame());
			return;
		}
*/
		// add frame unchanged
		
		putInQueue(frame);

	}

	/**
	 * adds threadID to frame.
	 * sends data from the switch's buffer to its destination
	 */
	public void send() {
		Integer port;

		// convert to Object
		if (frame == null)
			return;
		Frame f = new Frame(frame);
		if (f.getNetDest() > 0) {
			port = controller.allCAS[f.getNetDest() - 1].getPortNumber();
			// convert back to byte array
			frame = f.getFrame();
			// connect to node
			while (!connectToNode(port, 3))
				;
			// send
			NodeFileIO.writeBytesToSocket(nodeSocket, frame);
			NodeFileIO.closeSocket(nodeSocket);
		}
	}

	/**
	 * connects to a node
	 * @param port port of node
	 * @param waitTime time to wait on connection failure
	 * @return true if a connection was established, false otherwise
	 */
	private synchronized boolean connectToNode(int port, int waitTime) {

		try {
			nodeSocket = new Socket(this.inetAddress, port);
			return true;

		} catch (IOException e) {
			System.out.println("CCSTHREAD" + threadID +" failed to connect to the node\nTrying again in 2 seconds\n" + e.getMessage());
			try {
				sleep(waitTime * 1000);
			} catch (InterruptedException i) {
				System.out.println("CCSTHREAD" + threadID + " sleep time interrupted\n" + i.getMessage());
			}
			return false;
		}
	}
	/**
	 * connects to a node using an array of sockets
	 * @param index index of socket in array
	 * @param waitTime time to wait on connection failure
	 * @return true if a connection was established, false otherwise
	 */
	private boolean connectToNode(Socket socket, int waitTime, int index) {

		try {
			nodeSockets[index] = new Socket(this.inetAddress, socket.getPort());
			return true;

		} catch (IOException e) {
			System.out.println("CCSTHREAD" + threadID +" failed to connect to the switch\nTrying again in 2 seconds\n" + e.getMessage());
			try {
				sleep(waitTime * 1000);
			} catch (InterruptedException i) {
				System.out.println("CCSTHREAD" + threadID + " sleep time interrupted\n" + i.getMessage());
			}
			return false;
		}
	}
}
