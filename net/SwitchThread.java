package net;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * class that handles sending and receiving of the CCS and CAS
 */
public class SwitchThread extends Thread {

	private SwitchTask task;	// the task assigned to this thread
	private Socket nodeSocket;	// the socket of the node this thread is serving
	private Socket[] nodeSockets; // used for flooding
	private InetAddress inetAddress;
	private Switch controller; // the switch that spawned this thread
	private static long switchThreadID = 0; // Incremented and assigned to threadID with each SwitchThread that starts
	private static int nodeListeningPort = 6000;
	private int nodePort;
	private long threadID; // this data is added to the frame, 8 bytes between size and data.
	byte[] frame; // temp frame storage

	/**
	 * Constructor
	 * @param nodeSocket the socket connecting the switch to the node
	 * @param task the task of the switch
	 * @param controller the switch that the thread belongs to
	 */
	SwitchThread(Socket nodeSocket, SwitchTask task, Switch controller) {
		this.nodePort = ++nodeListeningPort;
		this.threadID = ++switchThreadID;
		this.task = task;
		this.nodeSocket = nodeSocket;
		this.controller = controller;
		try {
			inetAddress = InetAddress.getByName("localhost");
		}catch (IOException e) {
			e.getMessage();
		}
		
	}

	/**
	 * Constructor
	 * @param nodeSocket the socket connecting the switch to the node
	 * @param task the task of the switch
	 * @param controller the switch that the thread belongs to
	 * @param frame frame to be sent
	 */
	SwitchThread(Socket nodeSocket, SwitchTask task, Switch controller, byte[] frame) {
		this.nodePort = ++nodeListeningPort;
		this.threadID = ++switchThreadID;
		this.task = task;
		this.nodeSocket = nodeSocket;
		this.controller = controller;

		try {
			inetAddress = InetAddress.getByName("localhost");
		}catch (IOException e) {
			e.getMessage();
		}
		this.frame = new byte[Frame.framesize];
		System.arraycopy(frame, 0, this.frame, 0, frame.length);
	}

	/**
	 * start of the switch thread
	 */
	@Override
	public void run() {
		switch (task) {
			case RECEIVE:
				receive();
				NodeFileIO.closeSocket(nodeSocket);
				break;
			case SEND:
				send();
		}
	}

	/**
	 * takes the next item from the switch's buffer
	 * @return the next byte array from the switch's buffer, null if buffer is empty
	 */
	private synchronized byte[] takeFromQueue(){
		byte[] frame = new byte[Frame.framesize];
		try {
			if (!controller.queue.isEmpty())
				frame = controller.queue.take();
			else
				return null;
		} catch (InterruptedException e) {
			e.getMessage();
		}
		return frame;
	}

	/**
	 * puts a byte array (frame) into the switch's buffer
	 * @param frame frame to put in the buffer
	 */
	private synchronized void putInQueue(byte[] frame) {
		try {
			controller.queue.put(frame);
		} catch (InterruptedException e) {
			e.getMessage();
		}
	}

	/**
	 * accepts incoming connection
	 * @param socket socket that connects.
	 * @return the socket that connected
	 */
	private Socket acceptConnection(Socket socket, ServerSocket serverSocket, int port) {
		try {
			serverSocket = new ServerSocket(port);
			socket = serverSocket.accept();
			return socket;
		} catch (IOException e) {
			System.out.println("Thread" + threadID + " failed to accept connection\n" + e.getMessage());
			return null;
		}
	}

	//receive incoming data
	//check if the frame is an ack or not

	/**
	 * receives data from a node, adds to the switch's buffer
	 */
	public synchronized void receive() {
		byte[] frame = new byte[Frame.framesize];
		frame = NodeFileIO.readBytesFromSocket(nodeSocket, frame); // place data into temporary byte array
		Frame f = new Frame(frame);
		// add src to switching table and print confirmation.
		if (!controller.node2port.containsValue(f.getNodeSrc())) {
			if (f.getNodeSrc() > 0)
				nodePort = controller.allLinks[f.getNodeSrc() - 1].getListeningPort();

			if (controller.getNetID() == f.getNetSrc() && controller.addToSwitchingTable(nodePort, f.getNodeSrc())) {
				System.out.println("node " + f.getNetSrc() + "_" + controller.port2node.get(nodePort) + " added to network " + controller.getNetID() + " switching table at port " + controller.node2port.get(f.getNodeSrc()));
			}
		}
		// add frame unchanged
		// TODO: check which ack type
		if (isAck(frame)) {
			System.out.println("ACK received from node " + f.getNetSrc() +"_" + f.getNodeSrc() + " at CAS" + controller.getNetID() + " for node" + f.getNetDest() + "_" + f.getNodeDest() + " type: " + f.getAckType());
		}
		// add to queue
		putInQueue(frame);
	}

	/**
	 * sends data from the switch's buffer to its destination
	 */
	public void send() {
		Integer port;
		// convert to Object
		if (frame == null)
			return;
		Frame f = new Frame(frame);
		// if not in the right network
		if (f.getNetDest() != controller.getNetID()) {
			//forward to CCS
			while (!connectToNode(Switch.CCSport, 1))
				;
			NodeFileIO.writeBytesToSocket(nodeSocket, frame);
			NodeFileIO.closeSocket(nodeSocket);
		}
		else if (isAck(frame)) {
			// get destination
			port = controller.node2port.get(f.getNodeDest());
			// connect
			while (!connectToNode(port, 3)) ;
			// send
			NodeFileIO.writeBytesToSocket(nodeSocket, frame);
			NodeFileIO.closeSocket(nodeSocket);
		}
		else if ( (port = controller.node2port.get(f.getNodeDest())) != null) {
			// convert back to byte array
			frame = f.getFrame();
			// connect to node
			while (!connectToNode(port, 3)) ;
			// send
			NodeFileIO.writeBytesToSocket(nodeSocket, frame);
			NodeFileIO.closeSocket(nodeSocket);

		}

		// this is for flooding
		else {
			// used to determine how to close the socket(s)
			task = SwitchTask.FLOOD;
			// convert back to byte array
			frame = f.getFrame();
			// add port of socket to map of threadID -> socket
			for (int i = 0; i < controller.allLinks.length; i++){
				if (controller.allLinks[i] == null)
					continue;
				while (connectToNode(controller.allLinks[i].getListeningPort(),1))
					;
				NodeFileIO.writeBytesToSocket(nodeSocket, frame);
				NodeFileIO.closeSocket(nodeSocket);
			}
		}
	}

	/**
	 * connects to a node
	 * @param port port of node
	 * @param waitTime time to wait on connection failure
	 * @return true if a connection was established, false otherwise
	 */
	private synchronized boolean connectToNode(int port, int waitTime) {

		try {
			nodeSocket = new Socket(this.inetAddress, port);
			return true;

		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("SwitchThread" + threadID +" failed to connect to the node at port " + port + "\nTrying again in 2 seconds\n" + e.getMessage());
			try {
				sleep(waitTime * 1000);
			} catch (InterruptedException i) {
				System.out.println("SwitchThread" + threadID + " sleep time interrupted\n" + i.getMessage());
			}
			return false;
		}
	}

	/**
	 * connects to a node using an array of sockets
	 * @param waitTime time to wait on connection failure
	 * @return true if a connection was established, false otherwise
	 */
	private Socket connectToNode(Socket socket, int waitTime) {

		try {
			socket = new Socket(this.inetAddress, socket.getPort());
			return socket;

		} catch (IOException e) {
			//e.printStackTrace();
			System.out.println("SwitchThread" + threadID +" failed to connect to the switch\nTrying again in 2 seconds\n" + e.getMessage());
			try {
				sleep(waitTime * 1000);
			} catch (InterruptedException i) {
				System.out.println("SwitchThread" + threadID + " sleep time interrupted\n" + i.getMessage());
			}
			return null;
		}
	}
	
	/**
	 * check if the frame is an acknowledgement
	 * @param frame the frame
	 * @return true if it's an ACK, otherwise false
	 */
	private boolean isAck(byte[] frame) {
		return (frame[5] == (byte)0);
	}
}
