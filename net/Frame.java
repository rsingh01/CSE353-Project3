package net;
import java.io.*;
import java.nio.ByteBuffer;

/**
 *  This class is used for manipulating the frame.
 *  It can get the source, destination, size, and data (string) from the frame bytes
 *  It also can set the source, destination, and data and then get the frame bytes
 */
public class Frame
{
    private byte[] frame;
    private int[] src = new int[2];
    private int[] dest = new int[2];
    private int size;
    private int ackType;
    private byte[] data;
    private int crc;
    private int dataID;
    public static int framesize = 255 + 8;

    /**
     * empty constructor
     */
    public Frame() {}

    /**
     * constructor for frame class if we get bytes of data and need to figure our src/dest/data from what we have
     * @param input frame in byte form
     */
    public Frame(byte[] input)
    {
        frame = new byte[framesize];

        if (input == null)
            return;
        // byte convert to integer
        src[0] = (input[0] & 0xFF);
        src[1] = (input[1] & 0xFF);
        dest[0] = (input[2] & 0xFF);
        dest[1] = (input[3] & 0xFF);
        size = (input[4] & 0xFF);
        ackType = (input[5] & 0x03);
        dataID = (input[input.length - 2] & 0xFF);
        data = new byte[255];
        System.arraycopy(input, 6, data, 0, 255);

        crc = (input[input.length - 1] & 0xFF);
        frame = input;
    }

    /**
     * constructor for data as a string
     * @param input the line read from the file.
     * @param srcNetID The senders network id
     * @param srcNodeID the senders node id
     */
    public Frame(String input, int srcNetID, int srcNodeID, int dataID) {

        frame = new byte[framesize];

        String s = input.substring(0, input.indexOf(","));
        //System.out.println(s);
        String[] s2 = s.split("_");

        src[0] = srcNetID;
        src[1] = srcNodeID;

        dest[0] = Integer.parseInt(s2[0]);
        dest[1] = Integer.parseInt(s2[1]);

        ackType = 0;

        String d = input.substring(input.indexOf(",") + 1);

        data = d.getBytes();
        size = data.length;

        this.dataID = dataID;
        classToFrame();
        crc = 0;

        // if frame is an ACK, don't use CRC
        if (data.length == 0) {

        }

        for (int i = 0; i < frame.length - 1; i++) {
            crc += (frame[i] & 0xFF);
        }
        crc = (crc & 0xFF);
        frame[frame.length - 1] = (byte)crc;
    }

    /**
     * @return The frame's source (network and node id)
     */
    public int[] getSrc()
    {
        return src;
    }

    /**
     * @return The network id of this frame's source
     */
    public int getNetSrc(){return src[0];}

    /**
     * @return The node if of this frame's source
     */
    public int getNodeSrc(){return src[1];}

    /**
     * sets the source node of the frame
     * @param netID network id of the frame source
     * @param nodeID node id of the frame source
     */
    public void setSrc(int netID, int nodeID)
    {
        src[0] = netID;
        src[1] = nodeID;
        // recreate frame object with new value.
        classToFrame();
    }
    public int getDataID() {
        return this.dataID;
    }
    /**
     * @return The frame's destination (network and node id)
     */
    public int[] getDest()
    {
        return dest;
    }

    /**
     * @return The network id of this frame's destination
     */
    public int getNetDest(){return dest[0];}

    /**
     * @return The node id of this frame's destination
     */
    public int getNodeDest(){return dest[1];}

    /**
     * sets the destination node values
     * @param netID the network id of hte dest node
     * @param nodeID the node id of the dest node
     */
    public void setDest(int netID, int nodeID)
    {
        //sets the destination
        dest[0] = netID;
        dest[1] = nodeID;
        // recreate frame object with new value
        classToFrame();
    }

    /**
     * gets frame size
     * @return frame size
     */
    public int getSize()
    {
        return size;
    }

    public void setSize(int size) { this.size = size; }
    public int getCRC(){
        return crc;
    }
    public void setCRC(int crc){
        this.crc = crc;
    }
    /**
     * converts data to a string
     * @return a string of all of the frame's data
     */
    public String dataToString()
    {
        if (data == null)
            return null;
        else
            return new String(data);
    }

    /**
     * inserts data into the frame
     * @param d data string
     * @return true if set, false otherwise
     */
    public Boolean setData(String d)
    {
        if (d.length() > 255) {
            return false;
        }
        //set the length of the amount of data in the frame, and convert it into a frame object to send
        else {
            this.size = d.length();
            this.data = d.getBytes();
            classToFrame();
            return true;
        }
    }

    /**
     * gets the type of acknowledgment
     * @return the type of ack contained within the frame, null if frame is not an ack
     */
    public AckType getAckType() {
        if (size != 0) {
            // not an ack
            return null;
        }
        switch (ackType) {
            case 0:
                return AckType.TIMEOUT;
            case 1:
                return AckType.CRCERR;
            case 2:
                return AckType.FIREWALL;
            case 3:
                return AckType.POSITIVE;
        }
        return null;
    }

    /**
     * sets the acknowledgment type
     * @param ackType the appropriate value for the type of ack.
     */
    public void setAckType(AckType ackType) {
        switch (ackType) {
            case TIMEOUT:
                this.ackType = 0;
                break;
            case CRCERR:
                this.ackType = 1;
                break;
            case FIREWALL:
                this.ackType = 2;
                break;
            case POSITIVE:
                this.ackType = 3;
                break;
        }
    }

    /**
     * returns a frame object of the class
     * depends on classToFrame.
     */
    public byte[] getFrame()
    {
        classToFrame();
        return frame;
    }

    /**
     * converts a frame object into a byte array
     * used by getFrame before returning the frame in byte format
     */
    private void classToFrame()
    {
        frame = new byte[framesize];
        frame[0] = (byte)src[0];
        frame[1] = (byte)src[1];
        frame[2] = (byte)dest[0];
        frame[3] = (byte)dest[1];
        frame[4] = (byte)size;
        frame[5] = (byte)ackType;
        System.arraycopy(data, 0, frame, 6, data.length);
        frame[frame.length - 2] = (byte)dataID;
        frame[frame.length - 1] = (byte)crc;
    }

    /**
     * checks integrity of data via CRC
     * @return a positive AckType on success, CRCERR on failure
     */
    public AckType checkCRC() {
        int sum = 0;
        for (int i = 0; i < this.frame.length - 1; i++) {
            sum += this.frame[i];
        }
        sum = (sum & 0xFF);
        if (sum == crc) {
            return AckType.POSITIVE;
        }
        else {
            return AckType.CRCERR;
        }
    }
    public static Frame createAck (Frame frame, AckType ackType) {
        Frame ack = new Frame(frame.getFrame());

        // sets data as empty and size to 0
        ack.setData("");

        ack.setSrc(frame.getNetDest(), frame.getNodeDest());
        ack.setDest(frame.getNetSrc(), frame.getNodeSrc());
        ack.setAckType(ackType);
        return ack;
    }

    /** ** unused **
     * converts long to bytes
     * @param x long to convert
     * @return byte array of the long
     */
    public byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putLong(x);
        return buffer.array();
    }

    /** ** unused **
     * converts bytes to long
     * @param bytes bytes to convert to long
     * @return long value of the bytes
     */
    public long bytesToLong(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.put(bytes);
        buffer.flip();//need flip
        return buffer.getLong();
    }
}
